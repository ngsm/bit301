## Project Charter

**Project Title** : 

**Project Start Date:**

**Projected Finish Date:**

**Budget Information:**

**Project Manager:** 

Name:                 
E-mail:               


**Project Objectives:**



**Main Project Success Criteria:**



**Approach:**





**Roles and Responsibilities**


| Role | Name | Organization | Position | Email |
|------|------|--------------|----------|-------|
| Project Sponsor | Ng Shu Min     | HELP University  | Lecturer         | ngsm@help.edu.my       |
|      |      |              |          |       |
|      |      |              |          |       |

**Sign-off:** (Signatures of all above stakeholders. Can sign by their names in table above.)

**Comments:** (Handwritten or typed comments from above stakeholders, if applicable)